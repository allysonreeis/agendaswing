package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import agendaoo.Pessoa;

public class PessoaTest {
	
	Pessoa umaPessoa;
	
	@Before
	public void setUp() throws Exception {
		umaPessoa = new Pessoa ();
	}

	@Test
	public void testSetGetNome() {
		umaPessoa.setNome("Allyson");
		assertEquals("Allyson", umaPessoa.getNome());
	}

	@Test
	public void testSetGetIdade() {
		umaPessoa.setIdade("06/01/93");
		assertEquals("06/01/93", umaPessoa.getIdade());
	}

	@Test
	public void testSetGetTelefone() {
		umaPessoa.setTelefone("555-5555");
		assertEquals("555-5555", umaPessoa.getTelefone());
	}

	@Test
	public void testSetGetSexo() {
		umaPessoa.setSexo('M');
		assertEquals('M', umaPessoa.getSexo());
	}

	@Test
	public void testSetGetEmail() {
		umaPessoa.setEmail("testeEmail@gmail.com");
		assertEquals("testeEmail@gmail.com", umaPessoa.getEmail());
	}

	@Test
	public void testSetGetHangout() {
		umaPessoa.setHangout("umHangout");
		assertEquals("umHangout", umaPessoa.getHangout());
	}

	@Test
	public void testSetGetEndereco() {
		umaPessoa.setEndereco("UnB-Gama");
		assertEquals("UnB-Gama", umaPessoa.getEndereco());
	}

	@Test
	public void testSetGetRg() {
		umaPessoa.setRg("444.444.444");
		assertEquals("444.444.444", umaPessoa.getRg());
	}

	@Test
	public void testSetGetCpf() {
		umaPessoa.setCpf("444.444.444-44");
		assertEquals("444.444.444-44", umaPessoa.getCpf());
	}

}
